﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Login.Controlador
{
    class nombres
    {
        public string pedido { get; set; }
        public string Cliente { get; set; }
        public object JsonConvert { get; private set; }

        private const string RutaArchivo = @"C:\Persona.json";
        private void CargarInformacion()
        {
            pedido = JsonConvert.DeserializeObject<nombres>(File.ReadAllText(RutaArchivo));

            foreach (var prop in Cliente.GetType().GetProperties())
            {
                foreach (Control control in System.Windows.Controls)
                {
                    if (control.Name.Substring(3) != prop.Name || !prop.CanRead) continue;

                    if (control is TextBox)
                    {
                        //((TextBox)control).Text = prop.GetValue(Persona, null) as string;
                        ((TextBox)control).DataBindings.Add("Text", Cliente, prop.Name);
                    }
                }
            }
        }

        private void GuardarInformacion()
        {
            File.WriteAllText(RutaArchivo, JsonConvert.SerializeObject(Cliente));
        }

       
        
            
            
        

       
        
            
        
    }
}
    
